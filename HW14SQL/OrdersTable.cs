﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security;
using System.Data.SqlClient;
using NUnit.Framework;
using System.Data;


namespace TestSQL
{
    [[TestFixture]

    public class TestClassOrdersTable
    {
        SqlConnector connect = new SqlConnector("se", "se");

        [Test]
        public void TestOrdersMaximimSum()
        {
            connect.ConnectToCatalog("HW13");
            DataTable orders = connect.Execute($"SELECT * FROM Orders");
            decimal maxFromOrders = decimal.Parse(orders.Rows[0].ItemArray[2].ToString());
            for (int i = 0; i < orders.Rows.Count; i++)
            {
                if (maxFromOrders < decimal.Parse(orders.Rows[i].ItemArray[2].ToString()))
                {
                    maxFromOrders = decimal.Parse(orders.Rows[i].ItemArray[2].ToString());
                }
            }

            DataTable result = connect.Execute($"SELECT MAX(TotalPrice) AS maxOrder FROM Orders;");
            Assert.AreEqual(maxFromOrders, decimal.Parse(result.Rows[0].ItemArray[0].ToString()));
        }

        [Test]
        public void TestOrdersAverageSum()
        {
            connect.ConnectToCatalog("HW13");
            DataTable orders = connect.Execute($"SELECT * FROM Orders");
            decimal AVGFromOrdersSum = 0;

            for (int i = 0; i < orders.Rows.Count; i++)
            {
                AVGFromOrdersSum = AVGFromOrdersSum + decimal.Parse(orders.Rows[i].ItemArray[2].ToString());
            }
            decimal AVGFromOrders = AVGFromOrdersSum / orders.Rows.Count;
            DataTable result = connect.Execute($"SELECT AVG(Orders.TotalPrice) As AVG_ordersPrice FROM Orders");
            Assert.AreEqual(AVGFromOrders, decimal.Parse(result.Rows[0].ItemArray[0].ToString()));
        }

        [Test]
        public void TestOrdersMinimumSum()
        {
            connect.ConnectToCatalog("HW13");
            DataTable orders = connect.Execute($"SELECT * FROM Orders");
            decimal minFromOrders = decimal.Parse(orders.Rows[0].ItemArray[2].ToString());
            for (int i = 0; i < orders.Rows.Count; i++)
            {
                if (minFromOrders > decimal.Parse(orders.Rows[i].ItemArray[2].ToString()))
                {
                    minFromOrders = decimal.Parse(orders.Rows[i].ItemArray[2].ToString());
                }
            }

            DataTable result = connect.Execute($"SELECT MIN(TotalPrice) AS minOrder FROM Orders;");
            Assert.AreEqual(minFromOrders, decimal.Parse(result.Rows[0].ItemArray[0].ToString()));
        }

        [Test]
        public void CheckingThatOrdersAreOnlyFromRegisteredPersons()
        {
            bool flag = false;
            int count = 0;
            connect.ConnectToCatalog("HW13");
            DataTable resultOrders = connect.Execute("SELECT * FROM Orders");
            DataTable resultPersons = connect.Execute("SELECT * FROM Persons");
            for (int i = 0; i < resultPersons.Rows.Count; i++)
            {
                for (int j = 0; j < resultOrders.Rows.Count; j++)
                {
                    if (resultOrders.Rows[j].ItemArray[0].ToString() == resultPersons.Rows[i].ItemArray[0].ToString())
                    {
                        count++;

                    }
                    else
                    {
                        continue;
                    }
                }

            }
            if (count == resultOrders.Rows.Count)
            {
                flag = true;
            }
            Assert.AreEqual(true, flag);
        }


        [Test]
        public void TestTableColumnInOrders()
        {
            connect.ConnectToCatalog("HW13");
            DataTable result = connect.Execute("SELECT * FROM Orders");

            string ID = result.Columns[0].ColumnName.ToString();
            string PersonID = result.Columns[1].ColumnName.ToString();
            string TotalPrice = result.Columns[2].ColumnName.ToString();

            Assert.AreEqual("ID", ID);
            Assert.AreEqual("PersonID", PersonID);
            Assert.AreEqual("TotalPrice", TotalPrice);

        }
 
        [TestCase]
        public void CheckingOrderWithNullSum()
        {
            bool flag = false;
            connect.ConnectToCatalog("HW13");
            DataTable result = connect.Execute("SELECT * FROM Orders");
            for (int i = 0; i < result.Rows.Count; i++)
            {
                if (result.Rows[i].ItemArray[2].ToString().Length == 0)
                {
                    flag = true;
                    break;
                }
                else
                {
                    flag = false;
                }
            }
            Assert.AreEqual(false, flag);
        }



        [Test]
        public void CheckingThatTherAreOrdersInDB()
        {
            connect.ConnectToCatalog("HW13");
            DataTable result = connect.Execute("SELECT * FROM Orders");
            bool flag = false;

            if (result.Rows.Count > 0)
            {
                flag = true;

            }
            else
            {
                flag = false;
            }

            Assert.AreEqual(true, flag);

        }
    }
}

